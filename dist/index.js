"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuration
dotenv_1.default.config();
// Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// First Route
app.get('/', (req, res) => {
    res.send('WELCOME TO APP: Express + TS, + Swagger + Moongose');
});
app.get('/hello', (req, res) => {
    res.send('hola mundo');
});
// Execute APP
app.listen(port, () => {
    console.log(`EXPRESS IN SERVER: http://localhost:${port}/ `);
});
//# sourceMappingURL=index.js.map