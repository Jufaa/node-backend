import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv'

// Configuration

dotenv.config();

// Create Express APP
const app: Express = express()
const port: string | number = process.env.PORT || 8000;

// First Route

app.get('/', (req: Request,res: Response) => {
    res.send('WELCOME TO APP: Express + TS, + Swagger + Moongose')
})

app.get('/hello', (req: Request,res: Response) => {
    res.send('hola mundo')
})
// Execute APP

app.listen(port, () => {
    console.log(`EXPRESS IN SERVER: http://localhost:${port}/ `)
})